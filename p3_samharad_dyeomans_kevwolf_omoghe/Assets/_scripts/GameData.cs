﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData {
    public static int LeftSideScore { get; set; }

    public static int RightSideScore { get; set; }
}
